import script
import re

def process(msg):
    msg = re.sub('[^A-Za-z0-9\.]+', ' ', msg)
    print('Text with special characters striped: ', msg)
    return script.predict(msg)
