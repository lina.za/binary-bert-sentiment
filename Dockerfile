# This file is a template, and might need editing before it works on your project.
FROM registry.gitlab.com/lina.za/torch-base:latest


WORKDIR /app

COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /app
RUN wget --quiet https://www.dropbox.com/s/udcd6wyg74bmwqi/weights.zip
RUN unzip weights.zip

# For Django
EXPOSE 5000
CMD ["python", "service.py"]

# For some other command
# CMD ["python", "app.py"]

EXPOSE 8080 
ENTRYPOINT FLASK_APP=serve.py flask run --host=0.0.0.0 --port=8080